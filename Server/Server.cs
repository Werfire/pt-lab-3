﻿using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using NJsonSchema.Generation;
using NJsonSchema.Validation;
using SharedLibrary;

namespace Server;

class Server
{
    static async Task Main(string[] args)
    {
        try
        {
            await StartServer();
        }
        catch (IOException)
        {
            Console.WriteLine("Opponent disconnected. Game aborted.");
        }
        catch (JsonException)
        {
            Console.WriteLine("Opponent sent message in unexpected format. Game aborted.");
        }
        catch (Exception)
        {
            Console.WriteLine("Unknown error occured. Sorry, call administrator or something :(");
            throw;
        }
        finally
        {
            Console.ReadLine();
        }
    }

    static async Task StartServer()
    {
        var settings = new JsonSchemaGeneratorSettings();
        var generator = new JsonSchemaGenerator(settings);
        var schema = generator.Generate(typeof(GameState));

        bool isGameLoaded = false;
        string? gameStateJson = null;
        GameState gameState = new();

        bool isAnswerCorrect = false;
        while (!isAnswerCorrect)
        {
            Console.WriteLine("Do you want to load game from last saved state (y/n)?");
            var answer = Console.ReadLine();

            switch (answer.ToLower())
            {
                case "y":
                    isAnswerCorrect = true;

                    using (StreamReader streamReader = new("GameState.json")) 
                        gameStateJson = streamReader.ReadToEnd();

                    JsonSchemaValidator validator = new JsonSchemaValidator();
                    var validationErrors = validator.Validate(gameStateJson, schema);
                    if (validationErrors.Count > 0)
                    {
                        Console.WriteLine("Schema validation failed while loading last saved state. Starting game from initial state...");
                        gameStateJson = null;
                        break;
                    }

                    gameState = JsonSerializer.Deserialize<GameState>(gameStateJson);

                    isGameLoaded = true;
                    Console.WriteLine(ConstantValues.GameLoadedMessage);
                    break;
                case "n":
                    isAnswerCorrect = true;
                    Console.WriteLine("Game will be started from the beginning, waiting for client...");
                    break;
                default:
                    Console.WriteLine("Error, please provide valid response.");
                    break;
            }
        }

        IPAddress localAddr = IPAddress.Parse("127.0.0.1");
        var server = new TcpListener(localAddr, port: 1337);
        server.Start();
        TcpClient client = await server.AcceptTcpClientAsync();
        server.Stop();
        Console.WriteLine(ConstantValues.ConnectedMessage);
        NetworkStream stream = client.GetStream();

        byte[] buffer = new byte[1024];

        if (gameStateJson == null)
            gameStateJson = JsonSerializer.Serialize<GameState>(gameState);

        Helpers.WriteToBuffer(gameStateJson, buffer);
        await stream.WriteAsync(buffer, 0, buffer.Length);

        Console.WriteLine("Game begins!");

        bool isCreatingSequence = gameState.IsServerCreatingSequence;
        bool isFirstTurn = true;
        bool continueGame = true;

        while (continueGame)
        {
            if (isCreatingSequence)
            {
                if (isGameLoaded && gameState.Sequence != null && isFirstTurn)
                {
                    isFirstTurn = false;
                    goto MidTurn;
                }

                Console.Write(ConstantValues.RequestMessage);
                string sequence = Console.ReadLine().ToLower();
                if (sequence == null || sequence.Length != ConstantValues.SequenceLength ||
                    sequence.Any(color => !ConstantValues.AvailableColors.Contains(char.ToLower(color))))
                {
                    Console.WriteLine(ConstantValues.RewriteSequenceMessage);
                    continue;
                }

                Message message = new() { Sequence = sequence };
                string messageJson = JsonSerializer.Serialize(message);
                Helpers.WriteToBuffer(messageJson, buffer);
                await stream.WriteAsync(buffer, 0, buffer.Length);

                SaveGameState(new GameState(isCreatingSequence, sequence));

                MidTurn:
                
                Console.WriteLine(ConstantValues.WaitForResultMessage);
                await stream.ReadAsync(buffer, 0, buffer.Length);
                messageJson = Helpers.ReadFromBuffer(buffer);
                Signal? opponentResult = JsonSerializer.Deserialize<Message>(messageJson)?.Signal;

                if (opponentResult == null)
                    throw new JsonException();

                if (opponentResult == Signal.Lost)
                {
                    Console.WriteLine(ConstantValues.VictoryMessage);
                    continueGame = false;
                    continue;
                }

                if (opponentResult == Signal.GotItRight)
                {
                    Console.WriteLine(ConstantValues.TypedRightMessage);
                    isCreatingSequence = false;
                }

                SaveGameState(new GameState(isCreatingSequence, null));
            }
            else
            {
                string? sequence;

                if (isGameLoaded && gameState.Sequence != null && isFirstTurn)
                {
                    sequence = gameState.Sequence;
                    isFirstTurn = false;
                    goto MidTurn;
                }

                Console.WriteLine(ConstantValues.WaitForSequenceMessage);

                await stream.ReadAsync(buffer, 0, buffer.Length);
                string messageJson = Helpers.ReadFromBuffer(buffer);
                sequence = JsonSerializer.Deserialize<Message>(messageJson)?.Sequence;

                if (sequence == null)
                    throw new JsonException();

                SaveGameState(new GameState(isCreatingSequence, sequence));

                MidTurn:

                Console.WriteLine($"Memorize this sequence ({ConstantValues.MemorizeTime} seconds!): {sequence}");
                Thread.Sleep(ConstantValues.MemorizeTime * 1000);

                Console.Clear();
                Console.Write(ConstantValues.TypeRememberedMessage);
                string? recreatedSequence = Console.ReadLine();
                Message message;

                if (recreatedSequence == null || recreatedSequence.ToLower() != sequence)
                {
                    Console.WriteLine(ConstantValues.DefeatMessage);
                    message = new() { Signal = Signal.Lost };
                    messageJson = JsonSerializer.Serialize(message);
                    Helpers.WriteToBuffer(messageJson, buffer);
                    await stream.WriteAsync(buffer, 0, buffer.Length);
                    continueGame = false;
                    continue;
                }

                Console.WriteLine(ConstantValues.TypedRightMessage);
                message = new() { Signal = Signal.GotItRight };
                messageJson = JsonSerializer.Serialize(message);
                Helpers.WriteToBuffer(messageJson, buffer);
                await stream.WriteAsync(buffer, 0, buffer.Length);
                isCreatingSequence = true;

                SaveGameState(new GameState(isCreatingSequence, null));
            }
        }
    }

    private static void SaveGameState(GameState gameState)
    {
        using StreamWriter streamWriter = new("GameState.json");
        var gameStateJson = JsonSerializer.Serialize(gameState);
        streamWriter.Write(gameStateJson);
    }
}
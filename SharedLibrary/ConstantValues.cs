﻿namespace SharedLibrary;

public static class ConstantValues
{
    public static readonly char[] AvailableColors = { 'r', 'g', 'b', 'p', 'y', 'c' };

    public const int SequenceLength = 5;
    public const int MemorizeTime = 5;

    public static readonly string ConnectedMessage = "Successfully connected to opponent!";

    public static readonly string RequestMessage =
        $"Type a sequence of {SequenceLength} ball colors w/o spaces (use first letters of: Red, Green, Blue, Purple, Yellow, Cyan): ";

    public static readonly string TypeRememberedMessage = "Recreate sequence of ball colors you just remembered: ";

    public static readonly string RewriteSequenceMessage =
        "The sequence you provided is wrong! Please check the requirements and type it again...";

    public static readonly string WaitForSequenceMessage = "Wait for your opponent to provide the sequence, prepare to remember it!";

    public static readonly string WaitForResultMessage =
        "Wait until your opponent memorizes your sequence and types what he remembered...";

    public static readonly string TypedRightMessage =
        "Sequence is recreated right, switching sides...";

    public static readonly string VictoryMessage = "Your opponent made a mistake that means you are the winner!";

    public static readonly string DefeatMessage = "Oops, looks like you typed it wrong, you lost :(";

    public static readonly string GameLoadedMessage = "The game was loaded from last saved state.";
}


